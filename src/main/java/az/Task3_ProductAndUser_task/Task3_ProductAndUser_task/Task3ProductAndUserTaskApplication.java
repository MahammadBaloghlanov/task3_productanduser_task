package az.Task3_ProductAndUser_task.Task3_ProductAndUser_task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task3ProductAndUserTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task3ProductAndUserTaskApplication.class, args);
	}

}
